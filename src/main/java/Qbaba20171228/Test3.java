/**
 * Project Name:dt59homework
 * File Name:Test.java
 * Package Name:Qbaba20171228
 * Date:2017年12月28日上午11:14:33
 * Copyright (c) 2017, bluemobi All Rights Reserved.
 */

package Qbaba20171228;
/**
 * Description:   <br/>
 * Date:     2017年12月28日 上午11:14:33 <br/>
 * @author   Qbaba
 * @version题目：猴子吃桃问题：猴子第一天摘下若干个桃子，当即吃了一半
 * ，还不瘾，又多吃了一个 第二天早上又将剩下的桃子吃掉一半，又多吃了一个。
 * 以后每天早上都吃了前一天剩下 的一半零一个。
 * 到第10天早上想再吃时，见只剩下一个桃子了。求第一天共摘了多少。
 * @see
 */
public class Test3 {

    public static void main(String[] args) {
        int taoZi=1;int out = 0;
        for(int i=0;i<9;i++){
          out=(taoZi+1)*2;
            taoZi=out;
        }
      
         System.out.println("小猴子第一天摘了"+out+"个桃子！");

    }

}

